/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritmos;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author dalyn
 */
public class Polobio {
  static ArrayList<ArrayList> matriz = new ArrayList<>();
  static int[] pos = new int[2];
  static Scanner entrada = new Scanner(System.in);
  static String cifrado = "", cadena = "", s = ""; 
  static int aux;
  
  public Polobio(String men){
      cadena = men;
  }
  
  private String getfile(String file){
        try {
          Scanner input = new Scanner(new File(file));
          while (input.hasNextLine()) {
            s += input.nextLine();
          }
          input.close();
        } catch (Exception ex) {

        }
        return s;
    }
  public void polobio2()
  {
    
    aux = 0;
    String auxs="";
    for(int i = 0; i< cadena.length();i++){
        auxs+= String.valueOf(cadena.charAt(i)).toUpperCase();
    }
    cadena = auxs;
    for(int i = 0; i < cadena.length(); i++ ){
      if(!Character.isLetter(cadena.charAt(i)))
        cifrado += cadena.charAt(i);
      else{
        if(74 > cadena.charAt(i)){
          aux = cadena.charAt(i)-65;
          pos[0] = aux/5;
          pos[1] = aux%5;
        }
        else
        {
          aux = cadena.charAt(i)-66;
          pos[0] = aux/5;
          pos[1] = aux%5;
        }
        cifrado += pos[0] + "" + pos[1] ;
      }
    }
  }
    public String getC(){
    return cifrado;    
  }
  public String decodificar(){
    ArrayList<Integer> auxma = new ArrayList<>(); 
    ArrayList<Character> auxsim = new ArrayList<>();
    ArrayList<Character> auxma2 = new ArrayList<>();
    for(int i = 0; i < cadena.length(); i++ ){
        if(!Character.isLetter(cadena.charAt(i)))
            auxsim.add(cadena.charAt(i));
        else{
            auxma.add(Integer.parseInt(String.valueOf(cadena.charAt(i))));
        }
    }
    for(int i = 0; i < auxma.size(); i+= 2)
    {
        aux = ((auxma.get(i)*5)+auxma.get(i+1));
        if(aux <= 9)
          aux = aux+65;
        else
          aux = aux+66;
        auxma2.add((char)aux);
    }
    int count = 0;
    int si = 0, le = 0;
    while(count < cadena.length())
    {
        if(Character.isDigit(cadena.charAt(count))){
            cifrado += auxma2.get(le);
            le++;
            count +=2;
        }
        else{
            cifrado += auxsim.get(si);
            si++;
            count +=1;
            
        }
    }
    return cifrado;  
  }
  public static void mostrar()
  {
    for(int i = 0; i < 26; i++)
    { 
      aux = i+65;
      if (aux == 73){
        System.out.print((char)(aux) + "/" + (char)(aux+1)+ " ");
        i += 1;
        aux -= 1;
      }
      else{
        if(aux < 73)
          System.out.print((char)(aux) + " ");
        if(aux > 73){
          System.out.print((char)(aux) + " ");
          aux = aux-1;
        }
      }
      if((aux)%5 == 4)
        System.out.println();
    }
  }
}
