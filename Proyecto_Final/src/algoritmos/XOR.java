/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritmos;

import static algoritmos.Transposicion.s;
import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author dalyn
 */
public class XOR {
    public XOR(String m, char k){
        String men = getS(m);
        encryptDecrypt(men, k); 
    }
    
    static String getS(String file)
    {
        String men = "";
        try {
            Scanner input = new Scanner(new File(file));
            while (input.hasNextLine()) {
                s += input.nextLine();
            }
        }catch (Exception ex) {
            
        }
        return men;
    }
    static String encryptDecrypt(String inputString, char k) 
    { 
        char xorKey = k; 
        String outputString = ""; 
        
        int len = inputString.length(); 
  
        for (int i = 0; i < len; i++)  
        { 
            outputString = outputString +  
            Character.toString((char) (inputString.charAt(i) ^ xorKey)); 
        } 
  
        return outputString; 
    }
    
}
